cd enviroments/
cp .example.env.postgresql .env.postgresql 
cp .example.env.magneto_mutants .env.magneto_mutants
cd ..
docker-compose build
docker-compose up -d
docker-compose  exec app bundle
docker-compose  exec app rails db:create db:migrate