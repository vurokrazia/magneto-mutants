# README

# Magneto Mutants
## Antes de empezar
Requerimos tener en nuestro enviroment instalado lo siguiente
* Docker
* Docker compose

## Iniciando el setup
Copiar nuestros archivos de la carpeta enviroments removiendo la palabra example y escribiendo el valor de nuestras credenciales o dejar las credenciales de ejemplo.
``` bash
  cd enviroments/
  cp .example.env.postgresql .env.postgresql 
  cp .example.env.magneto_mutants .env.magneto_mutants
```
Creamos nuevas imagen base
``` bash
  docker-compose build
```
Levantamos servicios
``` bash
  docker-compose up -d
```
Creamos la base de datos y ejecutamos migraciones
``` bash
  docker-compose exec app rails db:create db:migrate
```
Tambien podemos ejecutar el siguiente comando si deseamos levantar todo el proyecto en un solo paso
``` bash
  bin/start.sh
```
## Services
* Servidor
  * Nombre del servicio: app
  * Port: 3000
  * Dependencias: database, sidekiq
* Postgres
  * Nombre del servicio: database
  * Image: postgres 13.2 
  * Dependencias: redis
* Redis
  * Nombre del servicio: redis
  * Image: redis alpine
* Sidekiq
  * Nombre del servicio: sidekiq
  * Dependencias: database
