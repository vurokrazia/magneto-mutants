# frozen_string_literal: true

if ENV['DOCKERIZE']
  Redis.current = Redis.new(
    db: ENV['REDIS_DB'],
    url: ENV['REDIS_URL'],
    port: ENV['REDIS_PORT'],
    password: ENV['REDIS_PASSWORD']
  )
else
  Redis.current = Redis.new(
    url: ENV['REDISCLOUD_URL'],
  )
end
