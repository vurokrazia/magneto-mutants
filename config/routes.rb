# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'welcome#index'
  get 'health', to: 'welcome#health'
  post 'mutant', to: 'dna#mutant'
  get 'stats', to: 'stat#stats'
end
