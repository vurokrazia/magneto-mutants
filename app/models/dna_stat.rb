# frozen_string_literal: true

# Model for mutant stadistics
class DnaStat < ApplicationRecord
  def ratio
    Rational(count_mutant_dna, count_human_dna).to_f.round(2)
  rescue ZeroDivisionError
    0
  end
end
