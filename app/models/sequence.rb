# frozen_string_literal: true

# Model for checker mutants
class Sequence < ApplicationRecord
  after_create :reload_stat

  validates_uniqueness_of :value
  validate :uniqueness_array

  private

  def uniqueness_array
    return if value.is_a?(Array)

    errors.add(:value,
               I18n.t(:uniqueness_array))
  end

  def reload_stat
    ::ReloadStats.new(self).update_stat
  end
end
