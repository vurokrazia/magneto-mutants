# frozen_string_literal: true

# Controller for check mutants
class DnaController < ApplicationController
  before_action :present_dna?
  after_action :background_sequence
  def mutant
    if dna_sequence
      head :ok
    else
      head :forbidden
    end
  end

  private

  def present_dna?
    return if params && params[:dna]

    head :forbidden
  end

  def params_dna
    params.permit(dna: [])
  end

  def sequence
    return @sequence if defined?(@sequence)

    @sequence = params[:dna]
  end

  def dna_sequence
    @dna_sequence ||= DnaSequence.isMutant?(sequence)
  end

  def background_sequence
    RegisterSequenceWorker.perform_async(sequence)
  end
end
