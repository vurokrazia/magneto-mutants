# frozen_string_literal: true

# Controller for muntant stats
class StatController < ApplicationController
  def stats
    render json: {
      count_mutant_dna: dna_stat.count_mutant_dna,
      count_human_dna: dna_stat.count_human_dna,
      ratio: dna_stat.ratio
    }
  end

  private

  def dna_stat
    @dna_stat ||= DnaStat.first_or_create
  end
end
