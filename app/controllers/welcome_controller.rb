# frozen_string_literal: true

# Controller for check health
class WelcomeController < ApplicationController
  def index
    head :no_content
  end

  def health
    render json: {}, status: :ok
  end
end
