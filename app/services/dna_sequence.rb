# frozen_string_literal: true

# Check if the DNA is mutant and human
class DnaSequence
  attr_reader :sequence

  def initialize(sequence)
    @sequence = sequence
  end

  def mutant?
    Mutants::Detector.new(builder).isMutant(sequence)
  rescue Mutants::Validations::InvalidSizeDnaSequence,
         Mutants::Validations::InvalidDNA
    false
  end

  def self.isMutant?(sequence)
    DnaSequence.new(sequence).mutant?
  end

  private

  def builder
    @builder ||= Mutants::Validations.new
                                     .set_sequence(
                                       sequence
                                     )
                                     .set_validate_sizes(true)
                                     .set_validate_dna(true)
  end
end
