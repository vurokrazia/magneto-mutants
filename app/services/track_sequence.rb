# frozen_string_literal: true

# Check uniq dna sequences on records
class TrackSequence
  attr_reader :sequence, :is_mutant

  def initialize(sequence, is_mutant)
    @sequence = sequence
    @is_mutant = is_mutant
  end

  def execute
    save
  end

  private

  def record
    @record ||= Sequence.new(
      value: sequence,
      is_mutant: is_mutant
    )
  end

  def save
    record.save if record.valid?
  end
end
