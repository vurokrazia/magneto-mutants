# frozen_string_literal: true

# Update DnaStats of mutants and humans
class ReloadStats
  attr_reader :dna_sequence

  def initialize(dna_sequence)
    @dna_sequence = dna_sequence
  end

  def update_stat
    if dna_sequence.is_mutant
      add_mutant
    else
      add_human
    end
  end

  private

  def stat
    @stat ||= DnaStat.first_or_create
  end

  def add_mutant
    stat.update(count_mutant_dna: 1 + stat.count_mutant_dna)
  end

  def add_human
    stat.update(count_human_dna: 1 + stat.count_human_dna)
  end
end
