# frozen_string_literal: true

# Analize DNA and save record
class RegisterSequenceWorker
  include Sidekiq::Worker
  attr_reader :sequence

  def perform(sequence)
    @sequence = sequence
    execute
  end

  def execute
    service.execute
  rescue Mutants::Validations::InvalidSizeDnaSequence,
         Mutants::Validations::InvalidDNA
  end

  def service
    @service ||= TrackSequence.new(sequence, mutant?)
  end

  def mutant?
    @mutant ||= DnaSequence.isMutant?(sequence)
  end
end
