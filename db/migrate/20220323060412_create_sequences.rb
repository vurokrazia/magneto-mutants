# frozen_string_literal: true

class CreateSequences < ActiveRecord::Migration[7.0]
  def change
    create_table :sequences do |t|
      t.jsonb :value
      t.boolean :is_mutant, default: false

      t.timestamps
    end
  end
end
