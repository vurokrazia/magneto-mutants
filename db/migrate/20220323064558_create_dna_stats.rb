# frozen_string_literal: true

class CreateDnaStats < ActiveRecord::Migration[7.0]
  def change
    create_table :dna_stats do |t|
      t.integer :count_mutant_dna, default: 0
      t.integer :count_human_dna, default: 0

      t.timestamps
    end
  end
end
