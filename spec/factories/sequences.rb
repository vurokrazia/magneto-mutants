# frozen_string_literal: true

FactoryBot.define do
  factory :sequence do
    trait :dna_human do
      value { %w[ACACCC CAGTAC TAATGT AGAAGG CACCAA TAGCGT] }
      is_mutant { false }
    end

    trait :dna_mutant do
      value { %w[ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG] }
      is_mutant { true }
    end
  end
end
