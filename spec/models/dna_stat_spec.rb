# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DnaStat, type: :model do
  describe '#ratio' do
    context 'when have number 0 on count_mutant_dna or count_human_dna' do
      it 'returns 0' do
        expect(described_class.first_or_create.ratio).to eql(0)
      end
    end
    context 'when have valid numbers on count_mutant_dna and count_human_dna' do
      it 'returns 0.4' do
        expect(
          described_class.create(count_mutant_dna: 40,
                                 count_human_dna: 100).ratio
        ).to eql(0.4)
      end
    end
  end
end
