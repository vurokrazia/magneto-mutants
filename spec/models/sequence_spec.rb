# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Sequence, type: :model do
  describe 'validations' do
    context 'when try duplicate value' do
      it 'return false' do
        described_class.create(value: %w[ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG])
        expect(
          described_class.new(value: %w[ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG]).save
        ).to be(false)
      end
    end
    context 'when try save value different to array' do
      it 'return false' do
        expect(
          described_class.new(value: 'ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG').save
        ).to be(false)
      end
    end
  end
  describe 'callbacks' do
    context 'when send dna human before create update stats' do
      it 'return true' do
        described_class.create(value: %w[ATGCGA CAGTGC TAATGT AGATGG TCCTA TCACAG])
        expect(DnaStat.first).not_to be(nil)
        expect(DnaStat.first.count_human_dna).to eql(1)
      end
    end
    context 'when send dna mutant before create update stats' do
      it 'return true' do
        described_class.create(value: %w[ATGCGA CAGTGC TTATGT AGAAGG CCCCTA TCACTG], is_mutant: true)
        expect(DnaStat.first).not_to be(nil)
        expect(DnaStat.first.count_mutant_dna).to eql(1)
      end
    end
  end
end
