# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReloadStats do
  describe 'update_stat' do
    context 'when send a sequence human' do
      it 'return true' do
        expect(
          described_class.new(
            FactoryBot.create(
              :sequence, :dna_human
            )
          ).update_stat
        ).to be true
        expect(
          DnaStat.first.count_human_dna
        ).to eql(2)
        expect(
          DnaStat.first.count_mutant_dna
        ).to eql(0)
      end
    end
    context 'when send a sequence mutant' do
      it 'return true' do
        expect(
          described_class.new(
            FactoryBot.create(
              :sequence, :dna_mutant
            )
          ).update_stat
        ).to be true
        expect(
          DnaStat.first.count_mutant_dna
        ).to eql(2)
        expect(
          DnaStat.first.count_human_dna
        ).to eql(0)
      end
    end
  end
end
