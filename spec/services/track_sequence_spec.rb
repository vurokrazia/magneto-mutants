# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TrackSequence do
  describe 'execute' do
    context 'when send a sequence mutant' do
      it 'return true' do
        subject = FactoryBot.build(
          :sequence, :dna_human
        )
        expect(
          described_class.new(
            subject.value,
            subject.is_mutant
          ).execute
        ).to be true
      end
    end
    context 'when try duplicate a sequence mutant' do
      it 'return nil' do
        FactoryBot.create(
          :sequence, :dna_human
        )
        subject = FactoryBot.build(
          :sequence, :dna_human
        )
        expect(
          described_class.new(
            subject.value,
            subject.is_mutant
          ).execute
        ).to be nil
      end
    end
  end
end
