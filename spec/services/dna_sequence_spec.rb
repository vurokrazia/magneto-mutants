# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DnaSequence do
  describe '.isMutant?' do
    context 'when send sequence is human' do
      it 'return nil' do
        expect(described_class.isMutant?(FactoryBot.build(
          :sequence, :dna_human
        ).value)).to be(nil)
      end
    end
    context 'when send sequence is mutant' do
      it 'return true' do
        expect(described_class.isMutant?(FactoryBot.build(
          :sequence, :dna_mutant
        ).value)).to be(true)
      end
    end
    context 'when send sequence with incorrect size' do
      it 'return false' do
        expect(described_class.isMutant?(%w[AAAAA AAAAA AAAA A])).to be(false)
      end
    end
    context 'when send sequence with incorrect DNA' do
      it 'return false' do
        expect(described_class.isMutant?(%w[HAAAAA AAAAA AAAAA AAAAA])).to be(false)
      end
    end
  end
  describe '#mutant?' do
    context 'when send sequence is human' do
      it 'return nil' do
        expect(described_class.new(FactoryBot.build(
          :sequence, :dna_human
        ).value).mutant?).to be(nil)
      end
    end
    context 'when send sequence is mutant' do
      it 'return true' do
        expect(described_class.new(FactoryBot.build(
          :sequence, :dna_mutant
        ).value).mutant?).to be(true)
      end
    end
    context 'when send sequence with incorrect size' do
      it 'return false' do
        expect(described_class.new(%w[AAAAA AAAAA AAAA A]).mutant?).to be(false)
      end
    end
    context 'when send sequence with incorrect DNA' do
      it 'return false' do
        expect(described_class.new(%w[HAAAAA AAAAA AAAAA AAAAA]).mutant?).to be(false)
      end
    end
  end
end
