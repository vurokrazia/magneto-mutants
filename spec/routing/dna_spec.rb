# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DnaController', type: :routing do
  describe 'routing dna controller' do
    it 'should exits route mutant' do
      expect(post: '/mutant').to route_to(
        'dna#mutant'
      )
    end
  end
end
