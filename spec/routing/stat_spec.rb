# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'StatController', type: :routing do
  describe 'routing stat controller' do
    it 'should exits route stats' do
      expect(get: '/stats').to route_to(
        'stat#stats'
      )
    end
  end
end
