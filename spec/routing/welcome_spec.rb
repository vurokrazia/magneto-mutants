# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Welcome', type: :routing do
  describe 'routing welcome controller' do
    it 'should exits route /' do
      expect(get: '/').to route_to(
        'welcome#index'
      )
    end
    it 'should exits route /health' do
      expect(get: '/health').to route_to(
        'welcome#health'
      )
    end
  end
end
