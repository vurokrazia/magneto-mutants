# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RegisterSequenceWorker, type: :worker, sidekiq: :fake do
  describe '.perform_async' do
    context 'when send a sequence human' do
      it 'should add process' do
        expect(
          described_class.perform_async(
            FactoryBot.build(
              :sequence, :dna_human
            ).value
          ).is_a?(String)
        ).to be(true)
      end
    end
  end
end
